'''
Author: Daniel Toth
https://gitlab.com/Daniel_Toth/binary-operations
'''

def add(a, b, overflow=True):
    n = abs(len(a) - len(b))
    if len(a) > len(b):
        b = n*'0' + b
    else:
        a = n*'0' + a
    a = a[::-1]
    b = b[::-1]
    carry = 0
    result = ''
    for i in range(0,len(a)):
        r = int(a[i]) + int(b[i]) + carry
        carry = 0
        if r>1:
            carry = 1
        result = str(r % 2) + result
    if carry and overflow:
        result = str(carry) + result
    return result

def complement(a):
    d=False
    a = list(a)
    for s in range(len(a)-1,-1,-1):
        if not d:
            if a[s] == '1':
                d = True
        else:
            if int(a[s]):
                a[s] = '0'
            else:
                a[s] = '1'
    a = "".join(a)
    return a

def subtraction(a, b):
    return add(a,complement(b),False)

def multiplication(a, b):
    res = []
    q = ''
    for s in range(len(a)-1, -1, -1):
        if int(a[s]):
            res.append(b + q)
        q += '0'
    multip = '0'
    for i in res:
        multip = add(i , multip)
    return multip

def baseconverter(base,n,tobase):
    n = n.upper()
    i = dec = 0
    jegyek = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ"
    for c in n:
        dec += jegyek.index(c)*base**(len(n)-i-1)
        i+=1
    res = ""
    while dec>0:
        res += jegyek[dec % tobase]
        dec = int(dec/tobase)
    return res[::-1]

def bitstofloat(n):
    n = n[-32:]
    n = (32-len(n))*"0" + n
    npbit = int(n[0:1])
    exponent = int(baseconverter(2,n[1:9],10)) - 127
    mantisa = n[9:32]
    ind = 1
    res = 1
    for i in mantisa:
        if int(i):
           res +=  .5**ind
        ind += 1

    return (-1)**npbit*res*2**exponent

def floattobits(n):
    #előjelbit meghatarozasa
    sign = "0"
    if n<0:
        sign = "1"
        n *= -1
    wholepart = int(n)
    fractionalpart = n - wholepart
    #egesz resz binaris kiszamolasa
    wholebits = baseconverter(10,str(wholepart),2)
    #tort resz binaris kiszamolasa
    fractionalbits = ""
    for i in range(0,28):
        fractionalpart *= 2
        if fractionalpart >= 1:
            fractionalpart -= 1
            fractionalbits +='1'
        else:
            fractionalbits +='0'
    mantissa = wholebits + fractionalbits
    ind = 0
    pos = -1
    # első "ertekes" bit helyzetenek megkeresese
    for i in mantissa:
        if (i == '1') and (pos == -1):
            pos = ind
        ind +=1
    #A bújtatott egyessel levágja az elejét
    mantissa = mantissa[pos+1:]
    exponent = 126 + len(wholebits) - pos
    exponent = baseconverter(10,str(exponent),2)
    #hatvany meretenek rogzitese
    if len(exponent)<8:
        exponent = (8-len(exponent))*"0" + exponent
    #mantissa "kerekitese", hogy a legkozelebbi leirhato ertekkel irja le
    if mantissa[23] == '1':
        mantissa = mantissa[:22] + '1'
    else:
        mantissa = mantissa[:23]

    print(sign + " | " + exponent[:8] + " | " + mantissa)
    return sign + exponent[:8] + mantissa


a = '001000110'
b = '100000010'

#print(" "+a+"\n+"+b+"\n-----------\n" + add(a,b))
#print("\n\n")
#print(" "+a+"\n-"+b+"\n-----------\n " + subtraction(a,b))
#print("\n\n")
#print(" "+a+"\n*"+b+"\n-----------\n " + str(multiplication(a,b)))

floatbits = input("Enter a float number:")
a = floattobits(float(floatbits))

#print(a)
print(bitstofloat(a))
